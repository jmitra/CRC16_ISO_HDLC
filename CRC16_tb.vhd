-------------------------------------------------------------------------------
-- Title      : CRC-16 FOR HDLC
-- Project    : CRU DCS 
-------------------------------------------------------------------------------
-- File       : CRC16_tb.vhd
-- Author     : Jubin MITRA
-- Contact	  : jubin.mitra@cern.ch
-- Company    : VECC, Kolkata, India
-- Created    : 08-01-2015
-- Last update: 08-01-2015
-- Platform   : 
-- Standard   : VHDL'93/08
-------------------------------------------------------------------------------
-- Description: 
-- Test Bench for CRC16.vhd							
-------------------------------------------------------------------------------
-- ****** CRC TYPE *******
-- Polynomial: G(x) = x^16 + x^12 + x^5 + 1
-- CRC Standard : CRC16-CCITT
-- Input Word Size : 16
-- Test value Reference : http://www.zorc.breitbandkatze.de/crc.html
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 08-01-2015  1.0      Jubin	Created
-------------------------------------------------------------------------------




--=================================================================================================--
--#################################################################################################--
--=================================================================================================--


-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all; 
USE ieee.STD_LOGIC_UNSIGNED.all; 
use ieee.numeric_std.all;


--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity CRC16_tb is

end CRC16_tb;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture behavioral of CRC16_tb is
 
--=================================================================================================--
						--========####  Component Declaration  ####========-- 
--=================================================================================================--   

component CRC16 is
   generic (
		constant INITIALIZATION_VALUE    		: std_logic_vector(15 downto 0):= x"FFFF";   
		constant INPUT_BYTE_REFLECT	    		: boolean:= true;
		constant OUTPUT_BYTE_REFLECT			: boolean:= true;
		constant XOROUT							: std_logic_vector(15 downto 0):= x"FFFF"
   );
   port (
      RESET_I                                   : in  std_logic;
      CLK_I   		                            : in  std_logic; 
	  SOF_I										: in std_logic;      
      DATA_I                					: in  std_logic_vector( 15 downto 0);
	  EOF_I										: in std_logic;
	  DATAVALID_I								: in std_logic;
      CHECKSUM_O					            : out std_logic_vector( 15 downto 0)
   );
end component;
	

--=================================================================================================--
						--========####  Signal Declaration  ####========-- 
--=================================================================================================--   
signal RESET									: std_logic := '0';
signal REF_CLK									: std_logic := '0';

signal SOF										: std_logic := '0';
signal EOF										: std_logic := '0';
signal DATA										: std_logic_vector(15 downto 0) := (others=>'0');
signal DATAVALID								: std_logic := '0';
signal CHECKSUM									: std_logic_vector(15 downto 0) := (others=>'0');


signal COUNTER					            	: std_logic_vector( 15 downto 0) := (others=>'0');



constant refclk_period	 						: time := 8.333 ms;			-- 120 MHz
constant wait_period		 					: time := 10    ms;			

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--  
   
   --==================================== Port Mapping ======================================--

   crc16_comp: 
   CRC16
      port map(
      RESET_I                                   => RESET,
      CLK_I   		                            => REF_CLK,
	  SOF_I										=> SOF,      
      DATA_I                					=> DATA,
	  EOF_I										=> EOF,
	  DATAVALID_I								=> DATAVALID,
      CHECKSUM_O					            => CHECKSUM
   );
   --==================================== Clock Generation =====================================--

   refclk_gen: process
   begin
		
		REF_CLK <= '1';
		wait for refclk_period/2;
		REF_CLK <= '0';
		wait for refclk_period/2;
   end process;

   --==================================== User Logic =====================================--

   
   counter_value_generator:
   process(RESET,REF_CLK)
   begin
		if RESET = '1' then
			COUNTER	<= (others => '0');
		elsif rising_edge(REF_CLK) then
			COUNTER <= COUNTER + '1' ;
		end if;
   end process;
   
   
   -- data_generator:
   -- process(RESET,REF_CLK)
      -- begin
		-- if RESET = '1' then
			-- DATA	<= (others => '0');
		-- elsif rising_edge(REF_CLK) then
			-- if COUNTER >= x"0020" and COUNTER <= x"0028" then
			-- DATA    	<= COUNTER ;
			
				-- if COUNTER = x"0020" then
					-- SOF			<= '1';
					-- DATAVALID	<= '1';
				-- elsif COUNTER = x"0021" then
					-- SOF			<= '0';
				-- elsif COUNTER = x"0027" then
					-- EOF			<= '1';
				-- elsif COUNTER = x"0028" then
					-- EOF			<= '0';
					-- DATAVALID	<= '0';
				-- end if;

			-- end if;
		-- end if;
   -- end process;
   
      data_generator:
   process(RESET,REF_CLK)
      begin
		if RESET = '1' then
			DATA	<= (others => '0');
		elsif rising_edge(REF_CLK) then
			if COUNTER >= x"0020" and COUNTER <= x"0028" then

				DATA    	<= COUNTER ;
			
				if COUNTER = x"0020" then
					SOF			<= '1';
					DATAVALID	<= '1';
				elsif COUNTER = x"0021" then
					SOF			<= '0';
					DATAVALID	<= '0';
				elsif COUNTER = x"0022" then
					DATAVALID	<= '1';
				elsif COUNTER = x"0027" then
					EOF			<= '1';
				elsif COUNTER = x"0028" then
					EOF			<= '0';
					DATAVALID	<= '0';
				end if;

			end if;
		end if;
   end process;
   
   
	test_proc:
	process
	begin
		RESET <= '1';
		wait for wait_period;
		RESET <= '0';
		wait for wait_period*10;
		wait;

	end process;
		
   --=====================================================================================--     
end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--