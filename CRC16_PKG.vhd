-------------------------------------------------------------------------------
-- Title      : CRC-16 FOR HDLC
-- Project    : CRU DCS 
-------------------------------------------------------------------------------
-- File       : CRC16_PKG.vhd
-- Author     : Jubin MITRA
-- Contact	  : jubin.mitra@cern.ch
-- Company    : VECC, Kolkata, India
-- Created    : 07-01-2015
-- Last update: 07-01-2015
-- Platform   : 
-- Standard   : VHDL'93/08
-------------------------------------------------------------------------------
-- Description: 
-- input  data of 16 bit each clock cycle
-- output checksum of 16 bit 								
-------------------------------------------------------------------------------
-- ****** CRC TYPE *******
-- Polynomial: G(x) = x^16 + x^12 + x^5 + 1
-- CRC Standard : CRC16-CCITT
-- Input Word Size : 16
-- Test value Reference : http://www.zorc.breitbandkatze.de/crc.html
-- For test data vector : [0x0020, 0x0021, 0x0022, 0x0023, 0x0024, 0x0025, 0x0026, 0x0027] response is 0xFB54
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 07-01-2015  1.0      Jubin	Created
-------------------------------------------------------------------------------



--=================================================================================================--
--#################################################################################################--
--=================================================================================================--


library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all; 
USE ieee.STD_LOGIC_UNSIGNED.all; 
use ieee.numeric_std.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

package CRC16_PKG is

subtype bit_vector16 is std_logic_vector(15 downto 0); 
type matrix16x16 is array (15 downto 0) of bit_vector16; 

constant syndrome_matrix : matrix16x16 :=(
										x"1B98",
										x"0DCC",
										x"06E6",
										x"0373",
										x"89A9",
										x"CCC4",
										x"6662",
										x"3331",
										x"9188",
										x"48C4",
										x"2462",
										x"1231",
										x"8108",
										x"4084",
										x"2042",
										x"1021");
										
function crc_compute (InputVector : bit_vector16)
                   return bit_vector16;

end CRC16_PKG;

package body CRC16_PKG is

--
--Description: Matrix multiplication: Output Checksum(1x16) =  Input vector (1x16) X  Syndrome Matrix (16x16)
--
function crc_compute (InputVector : bit_vector16)  return bit_vector16 is
  variable result : bit_vector16 := (others=>'0');
begin
		  for j in 15 downto 0 loop
			  for k in 15 downto 0 loop
				result(j) := result(j) xor (InputVector(k) and syndrome_matrix(k)(j)); --Standard notation: a(i,j) += b(i,k) x c(k,j) where i=1 in this case
			  end loop; 
		  end loop; 
	return result;
end crc_compute;


end CRC16_PKG;
