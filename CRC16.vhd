-------------------------------------------------------------------------------
-- Title      : CRC-16 FOR HDLC
-- Project    : CRU DCS 
-------------------------------------------------------------------------------
-- File       : CRC16_PKG.vhd
-- Author     : Jubin MITRA
-- Contact	  : jubin.mitra@cern.ch
-- Company    : VECC, Kolkata, India
-- Created    : 07-01-2015
-- Last update: 07-01-2015
-- Platform   : 
-- Standard   : VHDL'93/08
-------------------------------------------------------------------------------
-- Description: 
-- input  data of 16 bit each clock cycle
-- output checksum of 16 bit 								
-------------------------------------------------------------------------------
-- ****** CRC TYPE *******
-- Polynomial: G(x) = x^16 + x^12 + x^5 + 1
-- CRC Standard : CRC16-CCITT
-- Input Word Size : 16
-- Test value Reference : http://www.zorc.breitbandkatze.de/crc.html
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 07-01-2015  1.0      Jubin	Created
-------------------------------------------------------------------------------



--=================================================================================================--
--#################################################################################################--
--=================================================================================================--


library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all; 
USE ieee.STD_LOGIC_UNSIGNED.all; 
use ieee.numeric_std.all;

use work.CRC16_PKG.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--
entity CRC16 is
   generic (
		constant INITIALIZATION_VALUE    		: std_logic_vector(15 downto 0):= x"FFFF";   
		constant INPUT_BYTE_REFLECT	    		: boolean:= true;
		constant OUTPUT_BYTE_REFLECT			: boolean:= true;
		constant XOROUT							: std_logic_vector(15 downto 0):= x"FFFF"
   );
   port (
  
      --================--
      -- Reset & Clocks --
      --================--    
      
      -- Reset:
      ---------
      
      RESET_I                                    : in  std_logic;
  
      -- Clocks:
      ----------
      
      CLK_I   		                             : in  std_logic; 
      
      
      --==================--
      -- DATA BUS SIGNALS --
      --==================--
	  
	  SOF_I										: in std_logic;      
      DATA_I                					: in  std_logic_vector( 15 downto 0);
	  EOF_I										: in std_logic;
	  DATAVALID_I								: in std_logic;

      CHECKSUM_O					            : out std_logic_vector( 15 downto 0)	  
   
   );
end entity;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--
architecture behavioral of CRC16 is

   --==================================== Signal Definition =====================================--   

   signal checksum 								: std_logic_vector(15 downto 0)	 := (others=>'0');
   signal byte_reflected_data					: std_logic_vector(15 downto 0)	 := (others=>'0');
   signal pre_processed_data					: std_logic_vector(15 downto 0)	 := (others=>'0');
   signal bit_reflected_checksum				: std_logic_vector(15 downto 0)	 := (others=>'0');
   signal post_processed_checksum				: std_logic_vector(15 downto 0)	 := (others=>'0');
   
   signal eof_delayed_1_clock					: std_logic :='0';
   --=====================================================================================--  

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--

   --==================================== User Logic =============================================--   

   
   --============================================================================================--
   --================================# Byte Reflect Input Data #=================================--
   --============================================================================================--
input_byte_reflect_data: 
	for i in 0 to 7 generate
	
		gen_byte_reflect_data: 
		if INPUT_BYTE_REFLECT = true generate
			byte_reflected_data(7-i)					<=	DATA_I(i);
			byte_reflected_data(7-i+8)					<=	DATA_I(i+8);
		end generate gen_byte_reflect_data;
		
		gen_not_byte_reflect_data:
		if INPUT_BYTE_REFLECT = false generate	
			byte_reflected_data(i)						<=	DATA_I(i);
			byte_reflected_data(i+8)					<=	DATA_I(i+8);
		end generate gen_not_byte_reflect_data;
		
   end generate input_byte_reflect_data;
   
   
   --============================================================================================--
   --================================# Initialize reflected data  #=================================--
   --============================================================================================--
	pre_processed_data 									<= byte_reflected_data xor INITIALIZATION_VALUE when SOF_I='1'
														else byte_reflected_data xor checksum;


   --============================================================================================--
   --================================# Cyclic Code Computation #=================================--
   --============================================================================================--
	checksum_compute:
	process(RESET_I,CLK_I,DATAVALID_I)
	begin
		if RESET_I='1' then
		
			checksum 									<= (others=>'0');	
			
		elsif rising_edge(CLK_I) then
		
			if DATAVALID_I='1' then
			
				checksum           							<= crc_compute(pre_processed_data);
				
			end if;		
		end if;
	end process;

   --================================================================================================--
   --================================# Bit Reflect Output Checksum #=================================--
   --================================================================================================--
	output_bit_reflect_checksum: 
		for i in 0 to 15 generate
		
			gen_bit_reflect_checksum: 
			if OUTPUT_BYTE_REFLECT = true generate
				bit_reflected_checksum(15-i)				<=	checksum(i);
			end generate gen_bit_reflect_checksum;
			
			gen_not_bit_reflect_checksum:
			if OUTPUT_BYTE_REFLECT = false generate
				bit_reflected_checksum(i)					<=	checksum(i);
			end generate gen_not_bit_reflect_checksum;
			
	   end generate output_bit_reflect_checksum;

   --================================================================================================--
   --================================# XOR with reflected Checksum #=================================--
   --================================================================================================--
	
	post_processed_checksum									<= bit_reflected_checksum xor XOROUT;
	
   --================================================================================================--
   --================================# Latch CRC to Output Signal  #=================================--
   --================================================================================================--
	create_delayed_copy_eof:
	process(CLK_I)
	begin
		if rising_edge(CLK_I) then
			eof_delayed_1_clock								<= EOF_I;			
		end if;
	end process;
	

	CHECKSUM_O												<= post_processed_checksum when eof_delayed_1_clock = '1';

	
end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--