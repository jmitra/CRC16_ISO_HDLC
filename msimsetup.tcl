vlib work
vcom -2008 -work work CRC16_PKG.vhd
vcom -2008 -work work CRC16.vhd
vcom -2008 -work work CRC16_tb.vhd

vsim work.CRC16_tb
do wave.do
run 1000 ms