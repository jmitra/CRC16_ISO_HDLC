onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /crc16_tb/crc16_comp/RESET_I
add wave -noupdate -radix hexadecimal /crc16_tb/crc16_comp/CLK_I
add wave -noupdate -radix hexadecimal /crc16_tb/crc16_comp/SOF_I
add wave -noupdate -radix hexadecimal /crc16_tb/crc16_comp/DATA_I
add wave -noupdate -radix hexadecimal /crc16_tb/crc16_comp/EOF_I
add wave -noupdate -radix hexadecimal /crc16_tb/crc16_comp/DATAVALID_I
add wave -noupdate -radix hexadecimal /crc16_tb/crc16_comp/CHECKSUM_O
add wave -noupdate -divider {Internal Signals}
add wave -noupdate -radix hexadecimal /crc16_tb/crc16_comp/checksum
add wave -noupdate -radix hexadecimal /crc16_tb/crc16_comp/byte_reflected_data
add wave -noupdate -radix hexadecimal /crc16_tb/crc16_comp/pre_processed_data
add wave -noupdate -radix hexadecimal /crc16_tb/crc16_comp/bit_reflected_checksum
add wave -noupdate -radix hexadecimal /crc16_tb/crc16_comp/post_processed_checksum
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {170161264182 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {1060500 us}
